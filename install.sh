#!/bin/bash

cp -v bashrc $HOME/.bashrc
cp -v gitconfig $HOME/.gitconfig
cp -v tmux.conf $HOME/.tmux.conf
cp -v vimrc $HOME/.vimrc
cp -v gdbinit $HOME/.gdbinit

cp -v --parents .config/fish/functions/* $HOME

