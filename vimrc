set nocompatible " auf wiedersehen, vi

" tab stuff
set autoindent
set softtabstop=4 shiftwidth=4 expandtab

colors evening
" colors torte " colors for Dark Pastels scheme

" ascending search for tag file starting in current directory
set tags=./tags;

" settings for git commits, see best git commit practices:
" http://robots.thoughtbot.com/post/48933156625/5-useful-tips-for-a-better-commit-message
autocmd Filetype gitcommit setlocal spell textwidth=72

set nowrap " don't wrap lines
set hlsearch " highligh search matches
set nu " show line numbers
set textwidth=80 " 80 chars per line, affects various options
set formatoptions=c " auto-wrap comments and insert current comment leader!


" ctrl+hjkl split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" open new splits below and right
set splitbelow
set splitright

" auto save/load views
" views save lots of cool information about file like cursor pos and folds
" if having issues only with a specific file but not other files of the same
" type, try deleting the corresponding view file in ~/.vim/view
" auto save/load folds
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 

" sage settings
autocmd BufRead,BufNewFile *.sage,*.pyx,*.spyx set filetype=python
autocmd FileType python set makeprg=sage\ -b\ &&\ sage\ -t\ %

