python
import sys
import os
path = os.path.expanduser('~/dotfiles/python')
sys.path.insert(0, path)
from libstdcxx.v6.printers import register_libstdcxx_printers
register_libstdcxx_printers (None)
end
