function fish_prompt
	
    # add some cool git branch info to prompt when possible
    git status > /dev/null ^ /dev/null
    if test $status -eq 0
        set branch (git rev-parse --abbrev-ref HEAD)

        set diff (git diff origin/$branch ^ /dev/null)
        if test -n "$diff"
            set branch "$branch*"
        end

        set branch " ("(set_color magenta)"$branch"(set_color normal)")"
    end

    # build rest of prompt with name and time and dir
    set smiley "> "
    set me (set_color yellow)(whoami)(set_color normal)
    set host (set_color cyan)(hostname)(set_color normal)
    set path (set_color green)(pwd)(set_color normal)

    echo
    echo "[$me@$host] [$path]$branch"
    echo (date "+%I:%M ")"$smiley"
end
