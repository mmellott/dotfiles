function fish_greeting
    if test ! -n "$TMUX"
        # archer fortunes with a stoned cow
        fortune $HOME/usr/share/games/fortunes/archer | cowsay -s
    end
end
